//
//  Event.h
//  Neuronymus
//
//  Created by Luis Filipe Oliveira on 12/14/13.
//  Copyright (c) 2013 Luis Filipe Oliveira. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class EventField, Occurrence, Type;

@interface Event : NSManagedObject

@property (nonatomic, retain) NSNumber * averageDuration;
@property (nonatomic, retain) NSDate * createDate;
@property (nonatomic, retain) NSDate * estimateDate;
@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSNumber * recurrent;
@property (nonatomic, retain) NSDate * updateDate;
@property (nonatomic, retain) NSOrderedSet *eventFields;
@property (nonatomic, retain) NSOrderedSet *occurrences;
@property (nonatomic, retain) Type *type;
@end

@interface Event (CoreDataGeneratedAccessors)

- (void)insertObject:(EventField *)value inEventFieldsAtIndex:(NSUInteger)idx;
- (void)removeObjectFromEventFieldsAtIndex:(NSUInteger)idx;
- (void)insertEventFields:(NSArray *)value atIndexes:(NSIndexSet *)indexes;
- (void)removeEventFieldsAtIndexes:(NSIndexSet *)indexes;
- (void)replaceObjectInEventFieldsAtIndex:(NSUInteger)idx withObject:(EventField *)value;
- (void)replaceEventFieldsAtIndexes:(NSIndexSet *)indexes withEventFields:(NSArray *)values;
- (void)addEventFieldsObject:(EventField *)value;
- (void)removeEventFieldsObject:(EventField *)value;
- (void)addEventFields:(NSOrderedSet *)values;
- (void)removeEventFields:(NSOrderedSet *)values;
- (void)insertObject:(Occurrence *)value inOccurrencesAtIndex:(NSUInteger)idx;
- (void)removeObjectFromOccurrencesAtIndex:(NSUInteger)idx;
- (void)insertOccurrences:(NSArray *)value atIndexes:(NSIndexSet *)indexes;
- (void)removeOccurrencesAtIndexes:(NSIndexSet *)indexes;
- (void)replaceObjectInOccurrencesAtIndex:(NSUInteger)idx withObject:(Occurrence *)value;
- (void)replaceOccurrencesAtIndexes:(NSIndexSet *)indexes withOccurrences:(NSArray *)values;
- (void)addOccurrencesObject:(Occurrence *)value;
- (void)removeOccurrencesObject:(Occurrence *)value;
- (void)addOccurrences:(NSOrderedSet *)values;
- (void)removeOccurrences:(NSOrderedSet *)values;
@end
