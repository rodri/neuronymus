//
//  SideBarTopTableViewCell.h
//  Neuronymus
//
//  Created by Luis Filipe Oliveira on 12/12/13.
//  Copyright (c) 2013 Luis Filipe Oliveira. All rights reserved.
//



@interface SideBarTopTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIButton *editButton;
@property (weak, nonatomic) IBOutlet UIButton *addButton;

@end
