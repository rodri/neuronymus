//
//  Constants.h
//  Neuronymus
//
//  Created by Oliveira on 12/10/13.
//  Copyright (c) 2013 Luis Filipe Oliveira. All rights reserved.
//




//modal View controller types for use with modalViewcontroller protocols
typedef enum{
	ModalViewControllerTypeEventDetail,
    ModalViewControllerTypeOccurrenceDetail,
		
} ModalViewControllerType;




//add constants here and assign them in constants.m

//ios version - if compiling to ios  7 set to true
extern BOOL const kIOSVersion7;

//dataModel stuff
extern NSString *const kDataModelName;
extern NSString *const kDatabaseName;
//dataModel entities
extern NSString *const kEntityType ;
extern NSString *const kEntityEvent ;
extern NSString *const kEntityEventField ;
extern NSString *const kEntityOccurrence ;
extern NSString *const kEntityOccurrenceValue;
extern NSString *const kEntityDataType ;


// UITableViewcell reuse identifiers
extern NSString *const kSideBarTopCellId;
extern NSString *const kSideBarCellId;
extern NSString *const kEventsCellID;
extern NSString *const kTypesCellID;
