//
//  DataType.m
//  Neuronymus
//
//  Created by Luis Filipe Oliveira on 12/14/13.
//  Copyright (c) 2013 Luis Filipe Oliveira. All rights reserved.
//

#import "DataType.h"
#import "EventField.h"


@implementation DataType

@dynamic name;
@dynamic fields;

@end
