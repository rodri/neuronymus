
@class Type;
@class Event;
@class Occurrence;
@class EventField;
@class DataType;
@class OccurrenceValue;

@interface DataModel : NSObject


//access the datamodel singleton
+ (DataModel *)sharedDataModel;

//save all - use when quitting
- (void)saveCurrentContext;


//methods to perform database operations
- (void)createTypeWithName:(NSString *)name;
- (Type *)typeWithName:(NSString *)name;
- (NSArray *)allTypes;

- (Event *)addEventWithName:(NSString *)name toType:(Type *)type;
- (void)deleteEvent:(Event *)event;
- (Occurrence *)addOccurrenceForEvent:(Event *)event;
- (void)deleteOccurrence:(Occurrence *)occurrence;

- (EventField *)addFieldToEvent:(Event *)event withName:(NSString *)name andDataType:(DataType *)dataType;
- (OccurrenceValue *)addValue:(NSString *)value toField:(EventField *)field toOccurence:(Occurrence *)occurrence;
- (DataType *)createDataTypeWithName:(NSString *)name;
- (void)printAll;


//helper methods to mine data from the database
- (Occurrence *)lastOccurenceOfEvent:(Event *)event;
- (void)calculateStatsForEvent:(Event *)event;


@end
