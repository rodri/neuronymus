//
//  Constants.m
//  Neuronymus
//
//  Created by Oliveira on 12/10/13.
//  Copyright (c) 2013 Luis Filipe Oliveira. All rights reserved.
//

#import "Constants.h"

//assign constants here

//ios version - if compiling to ios  7 set to true
BOOL const kIOSVersion7 = YES;

//dataModel stuff
NSString *const kDataModelName = @"Neuronymus";
NSString *const kDatabaseName = @"Neuronymus.sqlite";
NSString *const kEntityType = @"Type";
NSString *const kEntityEvent = @"Event";
NSString *const kEntityEventField = @"EventField";
NSString *const kEntityOccurrence = @"Occurrence";
NSString *const kEntityOccurrenceValue = @"OccurrenceValue";
NSString *const kEntityDataType = @"Datatype";



NSString *const kSideBarTopCellId = @"sideBarTopCellId";
NSString *const kSideBarCellId = @"sideBarCellId";
NSString *const kEventsCellID = @"eventsCellId";
NSString *const kTypesCellID = @"typesCellId";
