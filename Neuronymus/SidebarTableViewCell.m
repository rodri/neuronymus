//
//  SidebarTableViewCell.m
//  Neuronymus
//
//  Created by Oliveira on 12/9/13.
//  Copyright (c) 2013 Luis Filipe Oliveira. All rights reserved.
//

#import "SidebarTableViewCell.h"

@implementation SidebarTableViewCell



//- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
//{
//    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
//    if (self) {
//    }
//    return self;
//}
//
//- (void)setSelected:(BOOL)selected animated:(BOOL)animated
//{
//    [super setSelected:selected animated:animated];
//
//    }

- (void)setEditing:(BOOL)editing animated:(BOOL)animated
{
    [super setEditing:editing animated:animated];
    
    if (editing){
        
        
        [UIView animateWithDuration:0.5
                              delay: 0.0
                            options: UIViewAnimationOptionCurveLinear
                         animations:^{
                             self.title.alpha = 0.0;
                             self.EditTitle.alpha = 1.0;
                             self.deleteButton.alpha = 1.0;
                            }
                         completion:nil
         ];
//
//        self.EditTitle.hidden = NO;
//        self.deleteButton.hidden = NO;
    }
    else{
        
        [self.EditTitle resignFirstResponder];
        
        [UIView animateWithDuration:0.5
                              delay: 0.0
                            options: UIViewAnimationOptionCurveLinear
                         animations:^{
                             self.title.alpha = 1.0;
                             self.EditTitle.alpha = 0.0;
                             self.deleteButton.alpha = 0.0;
                         }
                         completion:nil
         ];
       
//        self.EditTitle.hidden = YES;
//        self.deleteButton.hidden = YES;
    }
}

@end
