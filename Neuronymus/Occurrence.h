//
//  Occurrence.h
//  Neuronymus
//
//  Created by Luis Filipe Oliveira on 12/14/13.
//  Copyright (c) 2013 Luis Filipe Oliveira. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Event, OccurrenceValue;

@interface Occurrence : NSManagedObject

@property (nonatomic, retain) NSDate * alarmDate;
@property (nonatomic, retain) NSDate * createDate;
@property (nonatomic, retain) NSDate * finishDate;
@property (nonatomic, retain) NSDate * startDate;
@property (nonatomic, retain) NSDate * updateDate;
@property (nonatomic, retain) NSOrderedSet *details;
@property (nonatomic, retain) Event *event;
@end

@interface Occurrence (CoreDataGeneratedAccessors)

- (void)insertObject:(OccurrenceValue *)value inDetailsAtIndex:(NSUInteger)idx;
- (void)removeObjectFromDetailsAtIndex:(NSUInteger)idx;
- (void)insertDetails:(NSArray *)value atIndexes:(NSIndexSet *)indexes;
- (void)removeDetailsAtIndexes:(NSIndexSet *)indexes;
- (void)replaceObjectInDetailsAtIndex:(NSUInteger)idx withObject:(OccurrenceValue *)value;
- (void)replaceDetailsAtIndexes:(NSIndexSet *)indexes withDetails:(NSArray *)values;
- (void)addDetailsObject:(OccurrenceValue *)value;
- (void)removeDetailsObject:(OccurrenceValue *)value;
- (void)addDetails:(NSOrderedSet *)values;
- (void)removeDetails:(NSOrderedSet *)values;
@end
