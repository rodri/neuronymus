//
//  OccurrenceValue.m
//  Neuronymus
//
//  Created by Luis Filipe Oliveira on 12/14/13.
//  Copyright (c) 2013 Luis Filipe Oliveira. All rights reserved.
//

#import "OccurrenceValue.h"
#import "EventField.h"
#import "Occurrence.h"


@implementation OccurrenceValue

@dynamic value;
@dynamic name;
@dynamic occurrence;

@end
