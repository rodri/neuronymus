//
//  OccurrenceDetailTableViewController.h
//  Neuronymus
//
//  Created by Rodrigo Gomes on 12/11/13.
//  Copyright (c) 2013 Luis Filipe Oliveira. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Occurrence.h"

@interface OccurrenceDetailTableViewController : UITableViewController<ModalViewController, UITextFieldDelegate>

@property (nonatomic, strong) Occurrence *occurrence;

- (id)initWithOccurrence:(Occurrence *)occurrence;

@end
