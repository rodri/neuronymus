//
//  TypeSelectorViewController.h
//  Neuronymus
//
//  Created by Rodrigo Gomes on 12/14/13.
//  Copyright (c) 2013 Luis Filipe Oliveira. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Type.h"

@interface TypeSelectorViewController : UITableViewController

- (id)initWithType:(Type *__strong *)type;

@end
