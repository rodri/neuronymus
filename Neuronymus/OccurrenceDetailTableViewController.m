//
//  OccurrenceDetailTableViewController.m
//  Neuronymus
//
//  Created by Rodrigo Gomes on 12/11/13.
//  Copyright (c) 2013 Luis Filipe Oliveira. All rights reserved.
//

#import "OccurrenceDetailTableViewController.h"
#import "DetailTableViewCell.h"
#import "Event.h"
#import "Occurrence.h"
#import "UIColor+UIColor_CreateMethods.h"

@interface OccurrenceDetailTableViewController ()

@property (nonatomic, strong) NSArray *labels;
@property (nonatomic, strong) NSArray *detailItems;
@property (nonatomic, weak) UITextField *currentTextField;

@end

@implementation OccurrenceDetailTableViewController

//modalViewcontroller protocol implementation
@synthesize cancelled = _cancelled;
@synthesize modalDelegate = _modalDelegate;
@synthesize type = _type;

typedef enum{
	textFieldCreateDate,
    textFieldFinishDate,
    textFieldAlarmDate,
} textFieldTag;

- (id)initWithOccurrence:(Occurrence *)occurrence
{
    self = [super init];
    if (self) {
        
        self.occurrence = occurrence;
        _type = ModalViewControllerTypeOccurrenceDetail;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self reloadItems];

    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    
    // register our custom cell
    [self.tableView registerNib:[UINib nibWithNibName:@"DetailTableViewCell"
                                               bundle:[NSBundle mainBundle]]
         forCellReuseIdentifier:@"detailCellReuseID"];
    
    self.navigationItem.rightBarButtonItem = self.editButtonItem;
    
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    //_detailItems = @[@"Começou a", @"Termina a", @"Alarme"];
}

- (void)viewDidAppear:(BOOL)animated {
    
    [super viewDidAppear:animated];
    
    if (self.modalDelegate) {
        
        //cancel button
        UIBarButtonItem *cancelButton = [[UIBarButtonItem alloc]initWithTitle:@"Cancelar"
                                                                        style:UIBarButtonItemStylePlain
                                                                       target:self
                                                                       action:@selector(cancelAction)];
        //cancelButton.tintColor  = [UIColor colorWithHex:@"#F98500" alpha:1.0f];
        self.navigationItem.leftBarButtonItem = cancelButton;
        
        
        UIBarButtonItem *okButton = [[UIBarButtonItem alloc]initWithTitle:@"Guardar"
                                                                        style:UIBarButtonItemStylePlain
                                                                       target:self
                                                                       action:@selector(okAction)];
        self.navigationItem.rightBarButtonItem = okButton;
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [_detailItems count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"detailCellReuseID";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    UIDatePicker *datePicker = [[UIDatePicker alloc] init];
    datePicker.datePickerMode = UIDatePickerModeDateAndTime;
    [datePicker addTarget:self action:@selector(datePickerValueChanged:) forControlEvents:UIControlEventValueChanged];
    
    
    [((DetailTableViewCell *) cell).cellLabelEdit setText:[_labels objectAtIndex:indexPath.row]];
    [((DetailTableViewCell *) cell).cellLabel setText:[_labels objectAtIndex:indexPath.row]];
    [((DetailTableViewCell *) cell).cellValueEdit setText:[_detailItems objectAtIndex:indexPath.row]];
    [((DetailTableViewCell *) cell).cellValue setText:[_detailItems objectAtIndex:indexPath.row]];
    
    if (indexPath.row == 0) {
        [((DetailTableViewCell *) cell).cellValueEdit setTag:textFieldCreateDate];
        [datePicker setTag:textFieldCreateDate];
    }
    else if (indexPath.row == 1) {
        [((DetailTableViewCell *) cell).cellValueEdit setTag:textFieldFinishDate];
        [datePicker setTag:textFieldFinishDate];
    }
    else if (indexPath.row == 2) {
        [((DetailTableViewCell *) cell).cellValueEdit setTag:textFieldAlarmDate];
        [datePicker setTag:textFieldAlarmDate];
    }
    ((DetailTableViewCell *) cell).cellValueEdit.delegate = self;
    ((DetailTableViewCell *) cell).cellValueEdit.inputView = datePicker;
    
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return 80.0f;
}

- (BOOL)tableView:(UITableView *)tableView shouldHighlightRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return NO;
}

- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return UITableViewCellEditingStyleNone;
}

- (BOOL)tableView:(UITableView *)tableView shouldIndentWhileEditingRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return NO;
}

-(void)setEditing:(BOOL)editing animated:(BOOL)animated {
    
    if (editing) {
        [super setEditing:editing animated:animated];
        [self reloadDataAnimated];
    }
    else {
        
        if (self.modalDelegate) {
            
            [self.modalDelegate modalViewFinished:self];
        }
        else{
            
            [super setEditing:editing animated:animated];
            [self reloadItems];
            [self reloadDataAnimated];
        }
        
    }
}


/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a story board-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}

 */

#pragma mark - button Actions
- (void)cancelAction
{
    _cancelled = YES;
    [self.modalDelegate modalViewFinished:self];
}

- (void)okAction
{
    _cancelled = NO;
    [self.modalDelegate modalViewFinished:self];
}


#pragma mark - reload

- (void)reloadItems
{
    NSMutableArray *labels = [[NSMutableArray alloc] init];
    [labels addObject:@"Aconteceu a"];
    [labels addObject:@"Terminou a"];
    [labels addObject:@"Avisame a"];
    _labels = labels;
    
    NSMutableArray *details = [[NSMutableArray alloc] init];
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"dd-MM-yyyy HH:mm"];
    
    [details addObject:[formatter stringFromDate:_occurrence.startDate]];
    
    
    if (_occurrence.finishDate){
        
        [details addObject:[formatter stringFromDate:_occurrence.finishDate]];
    }
    else {
        
        [details addObject:@""];
    }
    
    [details addObject:[formatter stringFromDate:_occurrence.alarmDate]];
    _detailItems = details;
    
    self.navigationItem.title = self.occurrence.event.name;
}


//modalViewControllerDelegate protocol implementation
- (void)modalViewFinished:(id<ModalViewController>) controller
{
    [self dismissViewControllerAnimated:YES completion:NULL];
    
    if (controller.cancelled) return;
    
    
    // closed a event detail
    if (controller.type == ModalViewControllerTypeEventDetail)
    {
        //update event detail in the data model
    }
}

- (void)reloadDataAnimated
{
	[self.tableView  reloadSections:[NSIndexSet indexSetWithIndex:0] withRowAnimation:UITableViewRowAnimationFade];
}


#pragma mark - text field delegate

- (void)textFieldDidBeginEditing:(UITextField *)textField{

    _currentTextField = textField;
}


-(void) datePickerValueChanged:(id)sender {
    
    UIDatePicker *picker = sender;
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"dd-MM-yyyy HH:mm"];
    
    switch ([sender tag]) {
        case textFieldCreateDate:
            
            _occurrence.startDate = picker.date;
            break;
            
        case textFieldFinishDate:
            
            _occurrence.finishDate = picker.date;
            break;
            
        case textFieldAlarmDate:
            
            _occurrence.alarmDate = picker.date;
            break;
    }
    
    _currentTextField.text = [formatter stringFromDate:picker.date];
    
    //[self reloadItems];
    //[self.tableView reloadData];
}


@end
