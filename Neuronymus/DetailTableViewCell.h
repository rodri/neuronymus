//
//  DetailTableViewCell.h
//  Neuronymus
//
//  Created by Rodrigo Gomes on 12/11/13.
//  Copyright (c) 2013 Luis Filipe Oliveira. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DetailTableViewCell : UITableViewCell


@property (weak, nonatomic) IBOutlet UILabel *cellLabelEdit;
@property (weak, nonatomic) IBOutlet UILabel *cellLabel;
@property (weak, nonatomic) IBOutlet UITextField *cellValueEdit;
@property (weak, nonatomic) IBOutlet UILabel *cellValue;

@end
