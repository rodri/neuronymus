//
//  EventDetailViewController.m
//  Neuronymus
//
//  Created by Oliveira on 12/9/13.
//  Copyright (c) 2013 Luis Filipe Oliveira. All rights reserved.
//


#import "EventDetailViewController.h"
#import "UIColor+UIColor_CreateMethods.h"

@interface EventDetailViewController ()

@end

@implementation EventDetailViewController

//modalViewcontroller protocol implementation
@synthesize cancelled = _cancelled;
@synthesize modalDelegate = _modalDelegate;
@synthesize type = _type;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        //identify this controller 
        _type = ModalViewControllerTypeEventDetail;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    //navigation bar buttons
    UIBarButtonItem *cancelButton = [[UIBarButtonItem alloc]initWithTitle:@"Cancelar"
                                                                    style:UIBarButtonItemStylePlain
                                                                   target:self
                                                                   action:@selector(cancelAction)];
    //cancelButton.tintColor = [UIColor colorWithHex:@"#F98500" alpha:1.0f];
    self.navigationItem.leftBarButtonItem = cancelButton;
    self.navigationItem.hidesBackButton = YES;
    
    UIBarButtonItem *okButton = [[UIBarButtonItem alloc]initWithTitle:@"OK"
                                                                    style:UIBarButtonItemStylePlain
                                                                   target:self
                                                                   action:@selector(okAction)];
    //okButton.tintColor = [UIColor colorWithHex:@"#F98500" alpha:1.0f];
    self.navigationItem.rightBarButtonItem = okButton;
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - button Actions
- (void)cancelAction
{
    _cancelled = YES;
    [self.modalDelegate modalViewFinished:self];
}

- (void)okAction
{
    _cancelled = NO;
    [self.modalDelegate modalViewFinished:self];
   
}

@end
