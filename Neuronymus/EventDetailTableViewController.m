//
//  EventDetailTableViewController.m
//  Neuronymus
//
//  Created by Rodrigo Gomes on 12/11/13.
//  Copyright (c) 2013 Luis Filipe Oliveira. All rights reserved.
//

#import "EventDetailTableViewController.h"
#import "DetailTableViewCell.h"
#import "OccurrenceDetailTableViewController.h"
#import "Event.h"
#import "Type.h"
#import "DataModel.h"
#import "UIColor+UIColor_CreateMethods.h"
#import "TypeSelectorViewController.h"


@interface EventDetailTableViewController ()

@property (nonatomic, strong) NSArray *labels;
@property (nonatomic, strong) NSArray *detailItems;
@property (nonatomic, strong) NSArray *eventFields;
@property (nonatomic, strong) NSArray *occurrences;

@property (nonatomic, strong) Type *selectedType;

@end


@implementation EventDetailTableViewController

//modalViewcontroller protocol implementation
@synthesize cancelled = _cancelled;
@synthesize modalDelegate = _modalDelegate;
@synthesize type = _type;

int auxCellsNumber;
int editModeDetailItemsCount;

typedef enum{
	textFieldName,
    textFieldType
} textFieldTag;



- (id)initWithEvent:(Event *)event {
    
    self = [super init];
    
    if (self) {
        
        self.event = event;
        _type = ModalViewControllerTypeEventDetail;
    }
    return self;
    
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self reloadItems];

    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    
    
    // register our custom cell
    [self.tableView registerNib:[UINib nibWithNibName:@"DetailTableViewCell"
                                               bundle:[NSBundle mainBundle]]
         forCellReuseIdentifier:@"detailCellReuseID"];
    
    //self.editButtonItem.tintColor = [UIColor colorWithHex:@"#F98500" alpha:1.0f];
    self.navigationItem.rightBarButtonItem = self.editButtonItem;

    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    //self.editing = YES;
    
    
    auxCellsNumber = 2;
    editModeDetailItemsCount = 2;
   
}

- (void)viewWillAppear:(BOOL)animated {
    
    if (self.modalDelegate) {
        
        //cancel button
        UIBarButtonItem *cancelButton = [[UIBarButtonItem alloc]initWithTitle:@"Cancelar"
                                                                        style:UIBarButtonItemStylePlain
                                                                       target:self
                                                                       action:@selector(cancelAction)];
        self.navigationItem.leftBarButtonItem = cancelButton;
        
    }

    
    //edit button
    //self.navigationItem.rightBarButtonItem.title = @"Editar";
}

- (void)viewDidAppear:(BOOL)animated {
    
    [super viewDidAppear:animated];
    
    if (_modalDelegate){
        //creating a new event, so, create a occurrence as well
        Occurrence *occurrence = [[DataModel sharedDataModel] addOccurrenceForEvent:self.event];
        
    }

    
    
    if (_selectedType != nil) {
        
        _event.type = _selectedType;
    }
    
    [self reloadItems];
    [self reloadDataAnimated];
    
    }


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (self.editing) {
        
        return editModeDetailItemsCount + [_occurrences count];
    }
    
    return [_detailItems count] + [_occurrences count] + auxCellsNumber;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell;
    
    if (indexPath.row < [_detailItems count]) {
        
        static NSString *CellIdentifier = @"detailCellReuseID";
        cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
        [((DetailTableViewCell *) cell).cellLabelEdit setText:[_labels objectAtIndex:indexPath.row]];
        [((DetailTableViewCell *) cell).cellLabel setText:[_labels objectAtIndex:indexPath.row]];
        [((DetailTableViewCell *) cell).cellValue setText:[_detailItems objectAtIndex:indexPath.row]];
        [((DetailTableViewCell *) cell).cellValueEdit setText:[_detailItems objectAtIndex:indexPath.row]];
        
        if (indexPath.row == 0) {
            [((DetailTableViewCell *) cell).cellValueEdit setTag:textFieldName];
        }
        else {
            [((DetailTableViewCell *) cell).cellValueEdit setTag:textFieldType];
        }
        
        [((DetailTableViewCell *) cell).cellValueEdit setDelegate:self];
    }
    else if (indexPath.row == [_detailItems count]) {
        
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil];
        [cell.textLabel setText:@"Quando Aconteceu?"];
        cell.textLabel.textColor = [UIColor colorWithHex:@"#FFFFFF" alpha:1.0f];
        cell.backgroundColor = [UIColor colorWithHex:@"#F98500" alpha:1.0f];
        
    }
    else if (indexPath.row == [self tableView:self.tableView numberOfRowsInSection:0] -1) {
        
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil];
        [cell.textLabel setText:@"+ Aconteceu novamente"];
        cell.textLabel.textColor = [UIColor colorWithHex:@"#F98500" alpha:1.0f];
       
        
        
    }
    else {
        //Occurrences
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil];
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"dd-MM-yyyy HH:mm"];
        
        Occurrence *occ = [_occurrences objectAtIndex:(indexPath.row - [_detailItems count] - 1)];
        [cell.textLabel setText:[formatter stringFromDate:occ.startDate]];
        self.tableView.separatorColor = [UIColor colorWithHex:@"#D9D9D9" alpha:1.0];
    }
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.row < [_detailItems count]) {
        return 80.0f;
    }
    
    return 50.0f;
}


// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    return YES;
}

- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return UITableViewCellEditingStyleNone;
}

- (BOOL)tableView:(UITableView *)tableView shouldIndentWhileEditingRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return NO;
}

- (BOOL)tableView:(UITableView *)tableView shouldHighlightRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.row > [_detailItems count]) {
     
        return YES;
    }
    
    return NO;
}

// Override to support editing the table view.
/*- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        //[tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}*/


/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/


// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    return NO;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row > [_detailItems count] && indexPath.row != [self tableView:self.tableView numberOfRowsInSection:0] -1){
        
        
        Occurrence *occ = [self.occurrences objectAtIndex:(indexPath.row - [_detailItems count] - 1)];
        OccurrenceDetailTableViewController *cont = [[OccurrenceDetailTableViewController alloc] initWithOccurrence:occ];
        
        [self.navigationController pushViewController:cont animated:YES];
    }
    else if (indexPath.row == [self tableView:self.tableView numberOfRowsInSection:0] - 1){
        
        Occurrence *occurrence = [[DataModel sharedDataModel] addOccurrenceForEvent:self.event];
        
        OccurrenceDetailTableViewController *cont = [[OccurrenceDetailTableViewController alloc ]initWithOccurrence:occurrence];
        [cont setEditing:TRUE animated:YES];
        
        cont.modalDelegate = self;
        
        UINavigationController *nav = [[UINavigationController alloc]initWithRootViewController:cont];
        [self presentViewController:nav animated:YES completion:NULL];
    }
}

-(void)setEditing:(BOOL)editing animated:(BOOL)animated {
    
    if (editing) {
        editModeDetailItemsCount = 2;
        [super setEditing:editing animated:animated];
        [self reloadDataAnimated];
    }
    else {
        
        if (self.modalDelegate) {
            
            [self.modalDelegate modalViewFinished:self];
        }
        else{
        
            editModeDetailItemsCount = 5;
            [super setEditing:editing animated:animated];
            [self reloadItems];
            [self reloadDataAnimated];
        }
        
    }
}




/*
#pragma mark - Navigation

// In a story board-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}

 */

#pragma mark - button Actions
- (void)cancelAction
{
    _cancelled = YES;
    [self.modalDelegate modalViewFinished:self];
}

- (void)okAction
{
    _cancelled = NO;
    [self.modalDelegate modalViewFinished:self];
}

////modalViewControllerDelegate protocol implementation
//- (void)modalViewFinished:(id<ModalViewController>) controller
//{
//    [self dismissViewControllerAnimated:YES completion:NULL];
//    
//    if (controller.cancelled) return;
//    
//    
//    // closed a event detail
//    if (controller.type == ModalViewControllerTypeEventDetail)
//    {
//        //update event detail in the data model
//    }
//}

#pragma mark - reload

- (void)reloadItems
{
    
    
    NSMutableArray *labels = [[NSMutableArray alloc] init];
    [labels addObject:@"Nome"];
    [labels addObject:@"Categoria"];
    [labels addObject:@"Periodicidade Média"];
    [labels addObject:@"Último Evento"];
    [labels addObject:@"Data término prevista"];
    _labels = labels;
    
    NSMutableArray *details = [[NSMutableArray alloc] init];
    [details addObject:_event.name];
    
    if (_event.type) {
        [details addObject:_event.type.name];
    }
    else {
        [details addObject:@""];
    }
    
    
    
    if (!_event.occurrences || _event.occurrences.count == 0) {
        [details addObject:@"Não aplicável"];
      
        [details addObject:@"Não aplicável" ];
        [details addObject:@"Não aplicável"];
    }
    else{
        //update stats
        [[DataModel sharedDataModel]calculateStatsForEvent:self.event];
        
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"dd-MM-yyyy HH:mm"];
    
        [details addObject:[NSString stringWithFormat: @" %d dias",(int)(_event.averageDuration.intValue/86400)]];
        Occurrence *o = [[DataModel sharedDataModel]lastOccurenceOfEvent:self.event];
        [details addObject:[formatter stringFromDate:o.startDate] ];
        [details addObject:[formatter stringFromDate:_event.estimateDate]];
        //self.navigationItem.rightBarButtonItem.title = @"Editar";
    }
        _detailItems = details;
    
        _occurrences = [self.event.occurrences array];
        
        self.navigationItem.title = self.event.name;
    
    
}


- (void)reloadData
{
	[self.tableView reloadData];
}


- (void)reloadDataAnimated
{
	[self.tableView  reloadSections:[NSIndexSet indexSetWithIndex:0] withRowAnimation:UITableViewRowAnimationFade];
}


#pragma mark - text field delegate\

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    
    if (textField.tag == 1) {
        
        TypeSelectorViewController *cont = [[TypeSelectorViewController alloc] initWithType:(&_selectedType)];
        [self.navigationController pushViewController:cont animated:YES];
        
        return NO;
    }
    
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)aTextField{
    
    switch (aTextField.tag) {
        case textFieldName:
            
            _event.name = aTextField.text;
            
            break;
            
        case textFieldType: ;
            
            _event.type = [[DataModel sharedDataModel] typeWithName:aTextField.text];
    }
    
    NSLog(@"text: %@",aTextField.text);
    
    return true;
}

- (void)textFieldDidEndEditing:(UITextField *)atextField {
    
    switch (atextField.tag) {
        case textFieldName:
            
            _event.name = atextField.text;
            
            break;
            
        case textFieldType: ;
            
            _event.type = [[DataModel sharedDataModel] typeWithName:atextField.text];
            
    }
    
//    if (!self.editing) {
//        
//        [self reloadItems];
//        [self reloadDataAnimated];
//    }
    
    NSLog(@"text: %@",atextField.text);
}

//modalViewControllerDelegate protocol implementation
- (void)modalViewFinished:(id<ModalViewController>) controller
{
    [self dismissViewControllerAnimated:YES completion:NULL];
    
    if (controller.cancelled) {
        
        if (controller.type == ModalViewControllerTypeEventDetail) {
            
            Event *ev = ((EventDetailTableViewController *) controller).event;
            [[DataModel sharedDataModel] deleteEvent:ev];
        }
        else {
            Occurrence *occ = ((OccurrenceDetailTableViewController *) controller).occurrence;
            [[DataModel sharedDataModel] deleteOccurrence:occ];
        }
        
        return;
    }
    
    //update event detail in the data model
    [[DataModel sharedDataModel] saveCurrentContext];
    [self reloadItems];
    [self performSelector:@selector(reloadDataAnimated) withObject:nil afterDelay:0.3];
    
}



@end
