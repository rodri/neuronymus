//
//  DataModel.m
//  Neuronymus
//
//  Created by Luis Filipe Oliveira on 12/10/13.
//  Copyright (c) 2013 Luis Filipe Oliveira. All rights reserved.
//

#import "DataModel.h"
#import "Type.h"
#import "Event.h"
#import "Occurrence.h"
#import "EventField.h"
#import "OccurrenceValue.h"
#import "DataType.h"

// private interface
@interface DataModel()

@property (nonatomic,strong) NSManagedObjectContext *context;
@property (nonatomic, strong) NSManagedObjectModel *objectModel;
@property (nonatomic, strong) NSPersistentStoreCoordinator *coordinator;

- (void)initializeContext;
- (NSString *)getApplicationDocumentsDirectoryPath;
- (NSManagedObjectModel *)initializeManagedObjectModel;
- (NSPersistentStoreCoordinator *)initializeManagedPersistentStoreCoordinator;


@end



@implementation DataModel


+ (DataModel *)sharedDataModel
{
    static DataModel *dataModel;
    
    @synchronized(self) {
        if (dataModel == nil) {
            dataModel = [[DataModel alloc] init];
            [dataModel initializeContext];
        }
    }
    return dataModel;
}



- (NSString *)getApplicationDocumentsDirectoryPath
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *basePath = ([paths count] > 0) ? [paths objectAtIndex:0] : nil;
    return basePath;
}



- (NSManagedObjectModel *)initializeManagedObjectModel
{
    if (_objectModel == nil) {
       NSURL *modelURL = [[NSBundle mainBundle] URLForResource:kDataModelName withExtension:@"momd"];
        _objectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    }
    return _objectModel;
}



- (void)initializeContext{
    if (_context == nil) {
        NSPersistentStoreCoordinator *acoordinator = [self initializeManagedPersistentStoreCoordinator];
        _context = [[NSManagedObjectContext alloc] init];
        [_context setPersistentStoreCoordinator:acoordinator];
    }
}



-(NSPersistentStoreCoordinator *) initializeManagedPersistentStoreCoordinator{
    if (_coordinator == nil) {
        NSURL *storeUrl = [NSURL fileURLWithPath:[[self getApplicationDocumentsDirectoryPath]stringByAppendingPathComponent: kDatabaseName]];
        NSError *error = nil;
        _coordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self initializeManagedObjectModel]];
    
        if (![_coordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeUrl options:nil error:&error]) {
            NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
            abort();
        }
    }
    return _coordinator;
}



- (void)saveCurrentContext{
    NSError *error = nil;
    if (_context != nil) {
        if ([_context hasChanges] && ![_context save:&error]) {
            NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
            abort();
        }
    }
}


#pragma mark - database operations

- (void)createTypeWithName:(NSString *)name;
{
    Type *type = [NSEntityDescription insertNewObjectForEntityForName:kEntityType inManagedObjectContext:_context];
    type.name = name;
    type.createDate = [NSDate dateWithTimeIntervalSinceNow:0];
    type.updateDate = [NSDate dateWithTimeIntervalSinceNow:0];
   
    NSError *error = nil;
    if ([_context save:&error]) {
        NSLog(@"New Type saved");
    }
    else{
        NSLog(@"Error occured while saving Type: %@, %@", error, [error userInfo]);
    }
}



- (Type *)typeWithName:(NSString *)name
{
    NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:kEntityType];
    NSError *error = nil;
    //NSPredicate *predicate = [NSPredicate predicateWithFormat:@"name == %@", name];
    //[fetchRequest setPredicate:predicate];
    NSArray *objects = [_context executeFetchRequest:fetchRequest error:&error];
    
    if (!objects){
        NSLog(@"Error occured while fetching Type: %@, %@", error, [error userInfo]);
        return nil;
    }
    
    for (Type *t in objects){
        if ([t.name isEqualToString:name]){
            return t;
        }
    }
    return nil;
}



- (NSArray *)allTypes
{
    NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:kEntityType];
    NSError *error = nil;
    NSArray *objects = [_context executeFetchRequest:fetchRequest error:&error];
    
    if (!objects){
        NSLog(@"Error occured while fetching Types: %@, %@", error, [error userInfo]);
        return nil;
    }
    return objects;
}



- (Event *)addEventWithName:(NSString *)name toType:(Type *)type
{
    Event *event = [NSEntityDescription insertNewObjectForEntityForName:kEntityEvent inManagedObjectContext:_context];
    event.name = name;
    event.createDate = [NSDate dateWithTimeIntervalSinceNow:0];
    event.updateDate = [NSDate dateWithTimeIntervalSinceNow:0];
    event.type = type;
    
    NSError *error = nil;
    if ([_context save:&error]) {
        return event;
    }
    else{
        NSLog(@"Error occured while saving Event: %@, %@", error, [error userInfo]);
    }
    
    return nil;
}

- (Occurrence *)addOccurrenceForEvent:(Event *)event
{
    Occurrence *occurrence = [NSEntityDescription insertNewObjectForEntityForName:kEntityOccurrence inManagedObjectContext:_context];
    occurrence.createDate = [NSDate dateWithTimeIntervalSinceNow:0];
    occurrence.updateDate = [NSDate dateWithTimeIntervalSinceNow:0];
    occurrence.startDate = [NSDate dateWithTimeIntervalSinceNow:0];
    //occurrence.finishDate = [NSDate dateWithTimeIntervalSinceNow:0];
    occurrence.alarmDate = [NSDate dateWithTimeIntervalSinceNow:0];
    occurrence.event = event;
    
    NSError *error = nil;
    if ([_context save:&error]) {
        return occurrence;
    }
    else{
        NSLog(@"Error occured while saving Event: %@, %@", error, [error userInfo]);
    }
    
    return nil;
}

- (void)deleteEvent:(Event *)event {

    [_context deleteObject:event];
    
    NSError *error = nil;
    if (![_context save:&error]) {
        NSLog(@"Error occured while saving Event: %@, %@", error, [error userInfo]);
    }
}

- (void)deleteOccurrence:(Occurrence *)occurrence {
    
    [_context deleteObject:occurrence];
    
    NSError *error = nil;
    if (![_context save:&error]) {
        NSLog(@"Error occured while saving Event: %@, %@", error, [error userInfo]);
    }
}



- (EventField *)addFieldToEvent:(Event *)event withName:(NSString *)name andDataType:(DataType *)dataType
{
    EventField *field = [NSEntityDescription insertNewObjectForEntityForName:kEntityEventField inManagedObjectContext:_context];
    field.name = name;
    field.event = event;
    field.dataType = dataType;
    
    NSError *error = nil;
    if ([_context save:&error]) {
        return field;
    }
    else{
        NSLog(@"Error occured while saving Field: %@, %@", error, [error userInfo]);
    }
    
    return nil;
}


- (OccurrenceValue *)addValue:(NSString *)value toField:(EventField *)field toOccurence:(Occurrence *)occurrence
{
    OccurrenceValue *newValue = [NSEntityDescription insertNewObjectForEntityForName:kEntityOccurrenceValue inManagedObjectContext:_context];
    newValue.value = value;
    newValue.name = field;
    newValue.occurrence = occurrence;
    
    NSError *error = nil;
    if ([_context save:&error]) {
        return newValue;
    }
    else{
        NSLog(@"Error occured while saving Value: %@, %@", error, [error userInfo]);
    }
    
    return nil;
}



- (DataType *)createDataTypeWithName:(NSString *)name
{
    DataType *dataType = [NSEntityDescription insertNewObjectForEntityForName:kEntityDataType inManagedObjectContext:_context];
    dataType.name = name;
  
    NSError *error = nil;
    if ([_context save:&error]) {
        return dataType;
    }
    else{
        NSLog(@"Error occured while saving Datatype: %@, %@", error, [error userInfo]);
    }
    
    return nil;
}



//helper methods to mine data from the database
- (Occurrence *)lastOccurenceOfEvent:(Event *)event
{
    NSArray *occurrences = event.occurrences.array;
    NSDate *lastDate = [NSDate dateWithTimeIntervalSince1970:0]; // 1970!!!!!
    Occurrence *lastOccurrence;
    
    for (Occurrence *o in occurrences){
        if ([o.startDate compare:lastDate] == NSOrderedDescending){
            lastDate = o.startDate;
            lastOccurrence = o;
        }
    }
    return lastOccurrence;
}

- (void)calculateStatsForEvent:(Event *)event{
    if (!event.occurrences || event.occurrences.count == 0) return;
        
    NSArray *occurrences = [event.occurrences sortedArrayUsingComparator: ^(id obj1, id obj2) {
        return [((Occurrence *)obj2).startDate compare:((Occurrence *)obj1).startDate];
    }];
    // a soma de todos os males. Em segundos
    double sum = 0;
    
    for (int i = 0; i < occurrences.count-1 ; i++ ){
        sum += [((Occurrence *)[occurrences objectAtIndex:i]).startDate timeIntervalSinceDate:((Occurrence *)[occurrences objectAtIndex:i+1]).startDate];
    }
    
    Occurrence *lastOccurrence = [[DataModel sharedDataModel]lastOccurenceOfEvent:event];
    event.averageDuration = [NSNumber numberWithFloat:(sum/occurrences.count-1)];
    NSDate *estimate = [lastOccurrence.startDate dateByAddingTimeInterval:(sum/occurrences.count-1)];
    event.estimateDate = estimate;

}




- (void)printAll
{
    NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:kEntityType];
    NSError *error = nil;
    NSArray *objects = [_context executeFetchRequest:fetchRequest error:&error];
    
    if (!objects){
        NSLog(@"Error occured while fetching Types: %@, %@", error, [error userInfo]);
        return;
    }
    
    if (objects.count == 0){
        NSLog(@"no Types found");
        return;
    }
    
    for (Type *t in objects){
        NSLog(@"Type name: %@",t.name);
        for (Event *e in t.events){
            NSLog(@"----Event: %@",e.name);
        }
    }
}








@end
