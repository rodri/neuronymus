//
//  Event.m
//  Neuronymus
//
//  Created by Luis Filipe Oliveira on 12/14/13.
//  Copyright (c) 2013 Luis Filipe Oliveira. All rights reserved.
//

#import "Event.h"
#import "EventField.h"
#import "Occurrence.h"
#import "Type.h"


@implementation Event

@dynamic averageDuration;
@dynamic createDate;
@dynamic estimateDate;
@dynamic name;
@dynamic recurrent;
@dynamic updateDate;
@dynamic eventFields;
@dynamic occurrences;
@dynamic type;

@end
