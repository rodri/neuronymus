//
//  Occurrence.m
//  Neuronymus
//
//  Created by Luis Filipe Oliveira on 12/14/13.
//  Copyright (c) 2013 Luis Filipe Oliveira. All rights reserved.
//

#import "Occurrence.h"
#import "Event.h"
#import "OccurrenceValue.h"


@implementation Occurrence

@dynamic alarmDate;
@dynamic createDate;
@dynamic finishDate;
@dynamic startDate;
@dynamic updateDate;
@dynamic details;
@dynamic event;

@end
