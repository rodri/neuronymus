//
//  AppDelegate.m
//  Neuronymus
//
//  Created by Luis Filipe Oliveira on 12/7/13.
//  Copyright (c) 2013 Luis Filipe Oliveira. All rights reserved.
//

#import "AppDelegate.h"
#import "SidebarTableViewController.h"
#import "EventsTableViewController.h"
#import "SWRevealViewController.h"
#import "DataModel.h"
#import "UIColor+UIColor_CreateMethods.h"

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    // Override point for customization after application launch.
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]] ;
    [self.window setTintColor:[UIColor colorWithHex:@"#F98500" alpha:1.0f]];
    
    //start database
    DataModel *d = [DataModel sharedDataModel];
    NSArray* types = d.allTypes;

    
    SidebarTableViewController *sideVC = [[SidebarTableViewController alloc ]initWithStyle:UITableViewStylePlain];
    EventsTableViewController *eventsVC = [[EventsTableViewController alloc ]initWithType:types.firstObject];
    UINavigationController *nav = [[UINavigationController alloc]initWithRootViewController:eventsVC];
    
    SWRevealViewController *revealVC = [[SWRevealViewController alloc ]initWithRearViewController:sideVC frontViewController:nav];
   
    //ios 6 only
    if (!kIOSVersion7){
        nav.navigationBar.barStyle = UIBarStyleBlackTranslucent;
    }
    
    self.window.rootViewController = revealVC;
    [self.window makeKeyAndVisible];
    
//    //test database
//    DataModel *d = [DataModel sharedDataModel];
//    [d createTypeWithName:@"Pessoal"];
//    Type *t = [d typeWithName:@"Pessoal"];
//    [d addEventWithName:@"Casorio" toType:t];
//    [d addEventWithName:@"Appy Day" toType:t];
//
//    [d printAll];
    
    
    
    return YES;
}
							
- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    [[DataModel sharedDataModel] saveCurrentContext];
}

@end
