//
//  OccurrenceValue.h
//  Neuronymus
//
//  Created by Luis Filipe Oliveira on 12/14/13.
//  Copyright (c) 2013 Luis Filipe Oliveira. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class EventField, Occurrence;

@interface OccurrenceValue : NSManagedObject

@property (nonatomic, retain) NSString * value;
@property (nonatomic, retain) EventField *name;
@property (nonatomic, retain) Occurrence *occurrence;

@end
