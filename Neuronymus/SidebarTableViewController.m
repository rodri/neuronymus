//
//  SidebarTableViewController.m
//  
//
//  Created by Rodrigo Gomes on 12/8/13.
//  Copyright (c) 2013 Luis Filipe Oliveira. All rights reserved.
//

#import "SidebarTableViewController.h"
#import "SWRevealViewController.h"
#import "EventsTableViewController.h"
#import "SidebarTopTableViewCell.h"
#import "SidebarTableViewCell.h"
#import "DataModel.h"
#import "Type.h"
#import "UIColor+UIColor_CreateMethods.h"


@interface SidebarTableViewController ()

    @property (nonatomic, strong) NSArray *items;

@end

@implementation SidebarTableViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self reloadItems];
    
    // register our custom cells
    [self.tableView registerNib:[UINib nibWithNibName:@"SidebarTableViewCell"
                                               bundle:[NSBundle mainBundle]]
                               forCellReuseIdentifier:kSideBarCellId];
    
    [self.tableView registerNib:[UINib nibWithNibName:@"SideBarTopTableViewCell"
                                               bundle:[NSBundle mainBundle]]
                                forCellReuseIdentifier:kSideBarTopCellId];
    
    
    //self.tableView.rowHeight = 69;
    self.tableView.separatorColor = [UIColor colorWithHex:@"#F2F2F2" alpha:1.0f];
    //self.tableView.backgroundColor = [UIColor colorWithHex:@"#FFFFFF" alpha:1.0f];
    UIImageView *tempImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"logo_bg.png"]];
    
    
    [self.tableView.backgroundView addSubview:tempImageView];
    
    self.tableView.allowsSelectionDuringEditing = YES;
    
    if (kIOSVersion7){
        //avoid being under the status bar (iOS7 only)
        self.tableView.contentInset = UIEdgeInsetsMake(20.0f, 0.0f, 0.0f, 0.0f);
    
        //make separator simmetrical (iOS7 only)
        self.tableView.separatorInset = UIEdgeInsetsMake(0, 20, 0, 80);
    }
    

}


#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return [self.items count]+1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell;
    

    //first row is for add/edit
    if (indexPath.row == 0){
        
        cell = [tableView dequeueReusableCellWithIdentifier:kSideBarTopCellId forIndexPath:indexPath];
        
        
        UIButton *addButton  = ((SideBarTopTableViewCell *)cell).addButton;
        [addButton addTarget:self action:(@selector(addAction:)) forControlEvents:UIControlEventTouchUpInside];
        
        UIButton *editButton  = ((SideBarTopTableViewCell *)cell).editButton;
        [editButton addTarget:self action:(@selector(editAction:)) forControlEvents:UIControlEventTouchUpInside];
       
    }
    else{
        cell = [tableView dequeueReusableCellWithIdentifier:kSideBarCellId forIndexPath:indexPath];
        ((SidebarTableViewCell *)cell).title.text = ((Type *)[self.items objectAtIndex:indexPath.row-1]).name;
        
        UIButton *deleteButton = ((SidebarTableViewCell *)cell).deleteButton;
        deleteButton.tag = indexPath.row;
        [deleteButton addTarget:self action:(@selector(deleteAction:)) forControlEvents:UIControlEventTouchUpInside];
        
        UITextField *editTitle = ((SidebarTableViewCell *)cell).EditTitle;
        editTitle.tag = indexPath.row-1;
        editTitle.text = ((Type *)[self.items objectAtIndex:indexPath.row-1]).name;
        [editTitle setDelegate:self];
     
    }
    
    //customize selected cell background color
    UIView *bgColorView = [[UIView alloc] init];
    bgColorView.backgroundColor = [UIColor grayColor];
    bgColorView.layer.masksToBounds = YES;
    cell.selectedBackgroundView = bgColorView;
    return cell;
}


#pragma mark - Button Actions
- (void) deleteAction:(id) sender {
    
    NSLog(@"Clicked delete %ld", (long)((UIView *) sender).tag);
}


- (void) addAction:(id) sender {
    
    NSLog(@"Clicked add");
    //add show alertview
  UIAlertView *myAlertView = [[UIAlertView alloc] initWithTitle:@"Criar Categoria"
                                                              message:@"Insira o nome para uma nova categoria"
                                                             delegate:self
                                                    cancelButtonTitle:@"Cancelar"
                                                    otherButtonTitles:@"OK", nil];
        myAlertView.alertViewStyle = UIAlertViewStylePlainTextInput;
        myAlertView.tintColor = [UIColor colorWithHex:@"#F98500" alpha:1.0f];
        [myAlertView show];
        //[self.tableView cellForRowAtIndexPath:indexPath].selected = NO;
}


- (void) editAction:(id) sender {
    
    NSLog(@"Clicked edit");
    [self setEditing:!self.editing animated:true];
}


-(void)setEditing:(BOOL)editing animated:(BOOL)animated {
    
    
    [super setEditing:editing animated:animated];
    [self reloadItems];
    [self reloadDataAnimated];
    
}






#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 0){
    
    }
    else{
        //instantiate and show a new eventsTableVC
        EventsTableViewController *eventsVC = [[EventsTableViewController alloc ]initWithType:[self.items objectAtIndex:indexPath.row-1] ];
        
        UINavigationController* navController = (UINavigationController*)self.revealViewController.frontViewController;
        [navController setViewControllers: @[eventsVC] animated: NO ];
        [self.revealViewController setFrontViewPosition: FrontViewPositionLeft animated: YES];
    }
}



- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewCellEditingStyleNone;
}


- (BOOL)tableView:(UITableView *)tableView shouldIndentWhileEditingRowAtIndexPath:(NSIndexPath *)indexPath
{
    return NO;
}

- (BOOL)tableView:(UITableView *)tableView shouldHighlightRowAtIndexPath:(NSIndexPath *)indexPath {
    
    // first cell is not selectable
    if (indexPath.row == 0) return NO;
    return YES;
}





#pragma mark - UIAlertViewDelegate
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    NSString *name = [alertView textFieldAtIndex:0].text;
    if (buttonIndex == 1 && name && name.length != 0){
        NSLog(@"new type name %@",name);
        DataModel *d = [DataModel sharedDataModel];
        [d createTypeWithName: name];
        [self reloadItems];
        [self reloadDataAnimated];
    }
}


#pragma mark - reload


- (void)reloadItems
{
    DataModel *d = [DataModel sharedDataModel];
    self.items = d.allTypes;
}


- (void)reloadData
{
	[self.tableView reloadData];
}


- (void)reloadDataAnimated
{
	[self.tableView  reloadSections:[NSIndexSet indexSetWithIndex:0] withRowAnimation:UITableViewRowAnimationFade];
}


#pragma mark - textfield delegate

- (BOOL)textFieldShouldReturn:(UITextField *)aTextField{
    
    Type *t = [_items objectAtIndex:aTextField.tag];
    t.name = aTextField.text;
    return true;
}

- (void)textFieldDidEndEditing:(UITextField *)aTextField {
    
    Type *t = [_items objectAtIndex:aTextField.tag];
    t.name = aTextField.text;

}

@end
