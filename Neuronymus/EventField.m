//
//  EventField.m
//  Neuronymus
//
//  Created by Luis Filipe Oliveira on 12/14/13.
//  Copyright (c) 2013 Luis Filipe Oliveira. All rights reserved.
//

#import "EventField.h"
#import "DataType.h"
#import "Event.h"
#import "OccurrenceValue.h"


@implementation EventField

@dynamic name;
@dynamic event;
@dynamic value;
@dynamic dataType;

@end
