//
//  Protocols.h
//  Neuronymus
//
//  Created by Oliveira on 12/9/13.
//  Copyright (c) 2013 Luis Filipe Oliveira. All rights reserved.
//

@protocol ModalViewController;


// a protocol used by a modal view controller to inform its delegate it has finished
@protocol ModalViewControllerDelegate <NSObject>
- (void)modalViewFinished:(id<ModalViewController>) controller;
@end


// a protocol to be implemented by the modal view controllers
@protocol ModalViewController<NSObject>
@property(nonatomic, readonly)BOOL cancelled;
@property(nonatomic, readonly)ModalViewControllerType type;
@property(nonatomic, weak)id<ModalViewControllerDelegate> modalDelegate;
@end

