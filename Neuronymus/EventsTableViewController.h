//
//  EventsTableViewController.h
//  Neuronymus
//
//  Created by Rodrigo Gomes on 12/8/13.
//  Copyright (c) 2013 Luis Filipe Oliveira. All rights reserved.
//

@class Type;

@interface EventsTableViewController : UITableViewController<ModalViewControllerDelegate>

- (id)initWithType:(Type *)type;

@end
