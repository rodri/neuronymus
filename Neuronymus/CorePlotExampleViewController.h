//
//  CorePlotExampleViewController.h
//  Neuronymus
//
//  Created by Rodrigo Gomes on 12/10/13.
//  Copyright (c) 2013 Luis Filipe Oliveira. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CorePlot-CocoaTouch.h"

@interface CorePlotExampleViewController : UIViewController<CPTPlotDataSource, ModalViewController>

@end
