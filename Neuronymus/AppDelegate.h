//
//  AppDelegate.h
//  Neuronymus
//
//  Created by Luis Filipe Oliveira on 12/7/13.
//  Copyright (c) 2013 Luis Filipe Oliveira. All rights reserved.
//



@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
