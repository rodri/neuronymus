//
//  EventField.h
//  Neuronymus
//
//  Created by Luis Filipe Oliveira on 12/14/13.
//  Copyright (c) 2013 Luis Filipe Oliveira. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class DataType, Event, OccurrenceValue;

@interface EventField : NSManagedObject

@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) Event *event;
@property (nonatomic, retain) NSOrderedSet *value;
@property (nonatomic, retain) DataType *dataType;
@end

@interface EventField (CoreDataGeneratedAccessors)

- (void)insertObject:(OccurrenceValue *)value inValueAtIndex:(NSUInteger)idx;
- (void)removeObjectFromValueAtIndex:(NSUInteger)idx;
- (void)insertValue:(NSArray *)value atIndexes:(NSIndexSet *)indexes;
- (void)removeValueAtIndexes:(NSIndexSet *)indexes;
- (void)replaceObjectInValueAtIndex:(NSUInteger)idx withObject:(OccurrenceValue *)value;
- (void)replaceValueAtIndexes:(NSIndexSet *)indexes withValue:(NSArray *)values;
- (void)addValueObject:(OccurrenceValue *)value;
- (void)removeValueObject:(OccurrenceValue *)value;
- (void)addValue:(NSOrderedSet *)values;
- (void)removeValue:(NSOrderedSet *)values;
@end
