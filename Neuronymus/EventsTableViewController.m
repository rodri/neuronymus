//
//  EventsTableViewController.m
//  Neuronymus
//
//  Created by Rodrigo Gomes on 12/8/13.
//  Copyright (c) 2013 Luis Filipe Oliveira. All rights reserved.
//

#import "SWRevealViewController.h"
#import "EventsTableViewController.h"
#import "EventsTableViewCell.h"
#import "EventDetailTableViewController.h"
#import "CorePlotExampleViewController.h"
#import "OccurrenceDetailTableViewController.h"
#import "Type.h"
#import "Event.h"
#import "DataModel.h"
#import "UIColor+UIColor_CreateMethods.h"

@interface EventsTableViewController ()

@property(nonatomic, strong)Type *type;
@property (nonatomic, strong) NSArray *items;

@end

@implementation EventsTableViewController

- (id)initWithType:(Type *)type;
{
    self = [super initWithStyle:UITableViewStylePlain];
    if (self) {
        
        self.type = type;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self reloadItems];
    
    // Set navigation bar background color
   /* NSArray *ver = [[UIDevice currentDevice].systemVersion componentsSeparatedByString:@"."];
    if ([[ver objectAtIndex:0] intValue] >= 7) {
        self.navigationController.navigationBar.barTintColor = [UIColor colorWithHex:@"#F98500" alpha:1.0f];
        self.navigationController.navigationBar.translucent = NO;
    }else {
        self.navigationController.navigationBar.tintColor = [UIColor colorWithHex:@"#F98500" alpha:1.0f];
    }*/
    

    
    //menu button for the nav bar

    UIImage *buttonImage = [UIImage imageNamed:@"Categories"];
    UIBarButtonItem *menuButton = [[UIBarButtonItem alloc] initWithImage:buttonImage style:UIBarButtonItemStylePlain target:self.revealViewController action:@selector(revealToggle:)];

    
    //menuButton.tintColor = [UIColor colorWithHex:@"#F98500" alpha:1.0f];
    self.navigationItem.leftBarButtonItem = menuButton;
    //self.navigationItem.hidesBackButton = YES;
    
    
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(createEventButtonAction:)];
    //self.navigationItem.rightBarButtonItem.tintColor = [UIColor colorWithHex:@"#F98500" alpha:1.0f];
    
    // Set the gesture
    [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];

    // register our custom cell
    [self.tableView registerNib:[UINib nibWithNibName:@"EventsTableViewCell"
                                               bundle:[NSBundle mainBundle]]
                               forCellReuseIdentifier:kEventsCellID];
    
    
    self.tableView.rowHeight = 69;
    self.tableView.separatorColor = [UIColor colorWithHex:@"#D9D9D9" alpha:1.0];
    
    // make separators simmetrical (iOS7 only)
    if (kIOSVersion7){
        self.tableView.separatorInset = UIEdgeInsetsMake(0, 20, 0, 20);
    }
    
}

-(void) viewDidAppear:(BOOL)animated {
    
    [super viewDidAppear:animated];
    
    [self reloadDataAnimated];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.items.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kEventsCellID forIndexPath:indexPath];
    
    // Configure the cell...
    Event *e = [self.items objectAtIndex:indexPath.row];
    
    [((EventsTableViewCell *) cell).title setText:e.name];
    
    if (e.occurrences && e.occurrences.count > 1){
        [[DataModel sharedDataModel]calculateStatsForEvent:e];
        Occurrence *o = [[DataModel sharedDataModel]lastOccurenceOfEvent:e];
        NSDate *now=[NSDate dateWithTimeIntervalSinceNow:0];
        float amount = [now timeIntervalSinceDate:o.startDate];
        [((EventsTableViewCell *) cell).lastOccurrence setText:[NSString stringWithFormat:@"há %d dias", (int)(-amount/86400)+1 ]];
 
    }else{
        [((EventsTableViewCell *) cell).lastOccurrence setText:@""];
 
    }
    [((EventsTableViewCell *) cell).repetitionsCount setText:[NSString stringWithFormat:@"%dx",e.occurrences.count]];
    
    UIButton *plusButton = ((EventsTableViewCell *) cell).plusButton;
    plusButton.tag = indexPath.row;
    [plusButton addTarget:self action:(@selector(createOccurrenceButtonAction:)) forControlEvents:UIControlEventTouchUpInside];
    
    return cell;
}




- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
//    if (indexPath.row % 2 == 0) {
//        CorePlotExampleViewController *plot = [[CorePlotExampleViewController alloc] init];
//        plot.modalDelegate = self;
//        
//        UINavigationController *nav = [[UINavigationController alloc]initWithRootViewController:plot];
//        [self presentViewController:nav animated:YES completion:NULL];
//    }
//    else {
    
        EventDetailTableViewController *cont = [[EventDetailTableViewController alloc ]initWithEvent:[self.items objectAtIndex:indexPath.row]];
        [self.navigationController pushViewController:cont animated:YES];
    //}
}



//modalViewControllerDelegate protocol implementation
- (void)modalViewFinished:(id<ModalViewController>) controller
{
    [self dismissViewControllerAnimated:YES completion:NULL];
    
    if (controller.cancelled) {
        
        if (controller.type == ModalViewControllerTypeEventDetail) {
            
            Event *ev = ((EventDetailTableViewController *) controller).event;
            [[DataModel sharedDataModel] deleteEvent:ev];
        }
        else {
            Occurrence *occ = ((OccurrenceDetailTableViewController *) controller).occurrence;
            [[DataModel sharedDataModel] deleteOccurrence:occ];
        }
        
        return;
    }

    
    // closed a event detail
    //if (controller.type == ModalViewControllerTypeEventDetail)
    //{
        
        //update event detail in the data model
        [[DataModel sharedDataModel] saveCurrentContext];
        [self reloadItems];
        [self performSelector:@selector(reloadDataAnimated) withObject:nil afterDelay:0.3];
    //}
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a story board-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}

 */

#pragma mark - Create Actions

- (void) createEventButtonAction:(id) sender {
    
    NSLog(@"Clicked %d", ((UIView *) sender).tag);
    
    Event *event = [[DataModel sharedDataModel] addEventWithName:@"Novo Evento" toType:self.type];
    
    EventDetailTableViewController *cont = [[EventDetailTableViewController alloc ]initWithEvent:event];
    [cont setEditing:TRUE animated:YES];
    cont.modalDelegate = self;
    
    UINavigationController *nav = [[UINavigationController alloc]initWithRootViewController:cont];
    [self presentViewController:nav animated:YES completion:NULL];
}

- (void) createOccurrenceButtonAction:(id) sender {
    
    Event *event = [self.items objectAtIndex:[sender tag]];
    Occurrence *occurrence = [[DataModel sharedDataModel] addOccurrenceForEvent:event];
    
    OccurrenceDetailTableViewController *cont = [[OccurrenceDetailTableViewController alloc ]initWithOccurrence:occurrence];
    [cont setEditing:TRUE animated:YES];
    cont.modalDelegate = self;
    
    UINavigationController *nav = [[UINavigationController alloc]initWithRootViewController:cont];
    [self presentViewController:nav animated:YES completion:NULL];
}



#pragma mark - reload

- (void)reloadItems
{
    if (self.type){
        self.items = [self.type.events array];
        self.navigationItem.title = self.type.name;
    }
    else{
        self.items = [NSArray array];
        self.navigationItem.title = @"Eventos";
    }
}


- (void)reloadData
{
	[self.tableView reloadData];
}


- (void)reloadDataAnimated
{
	[self.tableView  reloadSections:[NSIndexSet indexSetWithIndex:0] withRowAnimation:UITableViewRowAnimationFade];
}



@end
