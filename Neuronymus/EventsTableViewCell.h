//
//  EventsTableViewCell.h
//  Neuronymus
//
//  Created by Rodrigo Gomes on 12/8/13.
//  Copyright (c) 2013 Luis Filipe Oliveira. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EventsTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *title;
@property (weak, nonatomic) IBOutlet UILabel *lastOccurrence;
@property (weak, nonatomic) IBOutlet UILabel *repetitionsCount;
@property (weak, nonatomic) IBOutlet UIButton *plusButton;

@end
