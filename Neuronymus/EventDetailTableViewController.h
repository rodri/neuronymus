//
//  Created by Rodrigo Gomes on 12/11/13.
//  Copyright (c) 2013 Luis Filipe Oliveira. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Event.h"

@interface EventDetailTableViewController : UITableViewController<ModalViewController, ModalViewControllerDelegate, UITextFieldDelegate>

@property (nonatomic, strong) Event *event;

- (id)initWithEvent:(Event *)event;

@end
