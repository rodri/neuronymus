//
//  DetailTableViewCell.m
//  Neuronymus
//
//  Created by Rodrigo Gomes on 12/11/13.
//  Copyright (c) 2013 Luis Filipe Oliveira. All rights reserved.
//

#import "DetailTableViewCell.h"

@implementation DetailTableViewCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}


- (void)setEditing:(BOOL)editing animated:(BOOL)animated {
    
    [super setEditing:editing animated:animated];
    
    if (editing) {
        
        [UIView animateWithDuration:0.5
                              delay: 0.0
                            options: UIViewAnimationOptionCurveLinear
                         animations:^{
                             self.cellLabelEdit.alpha = 1.0;
                             self.cellLabel.alpha = 0.0;
                             self.cellValueEdit.alpha = 1.0;
                             self.cellValue.alpha = 0.0;

                         }
                         completion:nil
                         ];
    
//        self.cellLabelEdit.hidden = NO;
//        self.cellLabel.hidden = YES;
//        self.cellValueEdit.hidden = NO;
//        self.cellValue.hidden = YES;
    }
    else {
        [self.cellValueEdit resignFirstResponder];
        [UIView animateWithDuration:0.5
                              delay: 0.0
                            options: UIViewAnimationOptionCurveLinear
                         animations:^{
                             self.cellLabelEdit.alpha = 0.0;
                             self.cellLabel.alpha = 1.0;
                             self.cellValueEdit.alpha = 0.0;
                             self.cellValue.alpha = 1.0;
                             
                         }
                         completion:nil
         ];
        
//        self.cellLabelEdit.hidden = YES;
//        self.cellLabel.hidden = NO;
//        self.cellValueEdit.hidden = YES;
//        self.cellValue.hidden = NO;
    }
}

@end
