//
//  DataType.h
//  Neuronymus
//
//  Created by Luis Filipe Oliveira on 12/14/13.
//  Copyright (c) 2013 Luis Filipe Oliveira. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class EventField;

@interface DataType : NSManagedObject

@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSSet *fields;
@end

@interface DataType (CoreDataGeneratedAccessors)

- (void)addFieldsObject:(EventField *)value;
- (void)removeFieldsObject:(EventField *)value;
- (void)addFields:(NSSet *)values;
- (void)removeFields:(NSSet *)values;

@end
