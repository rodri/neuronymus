//
//  SidebarTableViewCell.h
//  Neuronymus
//
//  Created by Oliveira on 12/9/13.
//  Copyright (c) 2013 Luis Filipe Oliveira. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SidebarTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *title;
@property (weak, nonatomic) IBOutlet UITextField *EditTitle;
@property (weak, nonatomic) IBOutlet UIButton *deleteButton;

@end
