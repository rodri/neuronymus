//
//  Type.m
//  Neuronymus
//
//  Created by Luis Filipe Oliveira on 12/14/13.
//  Copyright (c) 2013 Luis Filipe Oliveira. All rights reserved.
//

#import "Type.h"
#import "Event.h"


@implementation Type

@dynamic createDate;
@dynamic name;
@dynamic updateDate;
@dynamic events;

@end
