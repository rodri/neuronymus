//
//  main.m
//  Neuronymus
//
//  Created by Luis Filipe Oliveira on 12/7/13.
//  Copyright (c) 2013 Luis Filipe Oliveira. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
