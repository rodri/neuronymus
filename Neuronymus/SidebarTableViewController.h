//
//  SidebarTableViewController.h
//
//
//  Created by Rodrigo Gomes on 12/8/13.
//  Copyright (c) 2013 Luis Filipe Oliveira. All rights reserved.
//


@interface SidebarTableViewController : UITableViewController <UIAlertViewDelegate, UITextFieldDelegate>

@end
