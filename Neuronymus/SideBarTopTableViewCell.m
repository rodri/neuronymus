//
//  SideBarTopTableViewCell.m
//  Neuronymus
//
//  Created by Luis Filipe Oliveira on 12/12/13.
//  Copyright (c) 2013 Luis Filipe Oliveira. All rights reserved.
//

#import "SideBarTopTableViewCell.h"

@implementation SideBarTopTableViewCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
    

    // Configure the view for the selected state
}

@end
